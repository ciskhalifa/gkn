<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai) :
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = ($aep == 'salin') ? '' : $arey['wo_mark'];
} else {
    $cid = '';
}

?>
<div class="container-fluid">
    <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-label label-control">Jenis</label>
                <div class="col-label">
                    <select class="select2 form-control col-md-4 jenis" name="kode_jenis" id="kode_jenis" style="width:100%">
                        <option value="">- Pilihan -</option>
                        <?php
                        $n = (isset($arey)) ? $arey['kode_jenis'] : '';
                        $q = $this->Data_model->selectData('m_jenis', 'kode');
                        foreach ($q as $row) {
                            $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                            echo '<option data-id="' . $row->kode . '" data-singkatan="' . $row->singkatan . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama_jenis . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-label label-control">WO (Work Order)</label>
                <div class="col-label">
                    <input type="text" class="form-control input-sm col-md-4" placeholder="Work Order" name="kode_wo" id="kode_wo" value="<?= (isset($arey)) ? $arey['wo_mark'] : ''; ?>" data-error="wajib diisi" required readonly>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group" id="po_vendor">
                <div class="col-label">
                    <table class="table" id="multi_vendor" style="width: 100%">
                        <thead>
                            <tr>
                                <th style="text-align:center;width:20px">No</th>
                                <th>VENDOR</th>
                                <th>PROJECT ID</th>
                                <th>SITE</th>
                                <th>AREA</th>
                                <th>KOTA</th>
                                <th>SOW</th>
                                <th>TONASE</th>
                                <th colspan="3" style="text-align:center;" class="text-white btn-danger">PO VENDOR</th>
                                <th colspan="3" style="text-align:center;" class="text-white btn-success">PO GKN</th>
                            </tr>
                            <tr style="text-align:center;">
                                <th colspan="8"></th>
                                <th>Nilai Material</th>
                                <th>Delivery Material</th>
                                <th>Jasa Material</th>
                                <th>Nilai Material</th>
                                <th>Delivery Material</th>
                                <th>Jasa Material</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (empty($rowvendor)) { ?>
                                <tr id="row1">
                                    <td style="text-align:center">1</td>
                                    <td><input type="text" placeholder="Vendor" class="form-control" id="vendor1" name="vendor[]" required></td>
                                    <td><input type="text" placeholder="Project id" class="form-control" id="project_id1" name="project_id[]" required></td>
                                    <td><input type="text" placeholder="Site" class="form-control" id="site1" name="site[]" required></td>
                                    <td><input type="text" placeholder="Area" class="form-control" id="area1" name="area[]" required></td>
                                    <td><input type="text" placeholder="Kota" class="form-control" id="kota1" name="kota[]" required></td>
                                    <td><input type="text" placeholder="SOW" class="form-control" id="sow1" name="sow[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="tonase1" name="tonase[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="v_nilai1" name="v_nilai[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="v_deliv1" name="v_deliv[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="v_jasa1" name="v_jasa[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="g_nilai1" name="g_nilai[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="g_deliv1" name="g_deliv[]" required></td>
                                    <td><input type="number" placeholder="number" min="1" class="form-control" id="g_jasa1" name="g_jasa[]" required></td>
                                </tr>
                            <?php } else if (isset($rowvendor)) { ?>

                                <?php
                                    $no = 0;
                                    foreach ($rowvendor as $row) :
                                        ?>
                                    <tr id="row<?php echo ($no + 1); ?>">
                                        <td style="text-align:center"><?php echo ($no + 1); ?></td>
                                        <td><input type="text" placeholder="Vendor" class="form-control" id="vendor<?php echo ($no + 1); ?>" name="vendor[]" value="<?php echo $row->vendor; ?>" required></td>
                                        <td><input type="text" placeholder="Project id" class="form-control" id="project_id<?php echo ($no + 1); ?>" name="project_id[]" value="<?php echo $row->project_id; ?>" required></td>
                                        <td><input type="text" placeholder="Site" class="form-control" id="site<?php echo ($no + 1); ?>" name="site[]" value="<?php echo $row->site; ?>" required></td>
                                        <td><input type="text" placeholder="Area" class="form-control" id="area<?php echo ($no + 1); ?>" name="area[]" value="<?php echo $row->area; ?>" required></td>
                                        <td><input type="text" placeholder="Kota" class="form-control" id="kota<?php echo ($no + 1); ?>" name="kota[]" value="<?php echo $row->kota; ?>" required></td>
                                        <td><input type="text" placeholder="SOW" class="form-control" id="sow<?php echo ($no + 1); ?>" name="sow[]" value="<?php echo $row->sow; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="tonase<?php echo ($no + 1); ?>" name="tonase[]" value="<?php echo $row->tonase; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="v_nilai<?php echo ($no + 1); ?>" name="v_nilai[]" value="<?php echo $row->v_nilai; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="v_deliv<?php echo ($no + 1); ?>" name="v_deliv[]" value="<?php echo $row->v_deliv; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="v_jasa<?php echo ($no + 1); ?>" name="v_jasa[]" value="<?php echo $row->v_jasa; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="g_nilai<?php echo ($no + 1); ?>" name="g_nilai[]" value="<?php echo $row->g_nilai; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="g_deliv<?php echo ($no + 1); ?>" name="g_deliv[]" value="<?php echo $row->g_deliv; ?>" required></td>
                                        <td><input type="number" min="1" placeholder="number" class="form-control" id="g_jasa<?php echo ($no + 1); ?>" name="g_jasa[]" value="<?php echo $row->g_jasa; ?>" required></td>
                                    </tr>
                                <?php
                                        $no++;
                                    endforeach;
                                    ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <button type="button" class="btn btn-primary addvendor">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger removevendor">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-label label-control">Dokumen</label>
        <div class="col-label">
            <input type="file" accept="application/pdf" class="form-control input-sm col-md-4" placeholder="Work Order" name="file_marketing" id="file" value="<?= (isset($arey)) ? $arey['file_marketing'] : ''; ?>" data-error="wajib diisi" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" id="saveform" class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
        <button type="button" id="cancelform" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#kode_jenis").on('change', function() {
            $.ajax({
                url: "<?= base_url('order/getKode'); ?>",
                type: "POST",
                data: "jenis=" + $(this).find(":selected").attr('data-singkatan') + "&kode_jenis=" + $(this).find(":selected").attr('data-id'),
                dataType: "html",
                success: function(html) {
                    $("#kode_wo").val(html);
                }
            });
        });
        var a = $("#multi_vendor tbody tr").length;

        $(".addvendor").on("click", function() {
            a++;
            var x = '<td style="text-align: center;">' + (a) + "</td>";
            x += '<td><input type="text" placeholder="Vendor" class="form-control" id="vendor' + (a) + '" name="vendor[]" required></td>';
            x += '<td><input type="text" placeholder="Project id" class="form-control" id="project_id' + (a) + '" name="project_id[]" required></td>';
            x += '<td><input type="text" placeholder="Site" class="form-control" id="site' + (a) + '" name="site[]" required></td>';
            x += '<td><input type="text" placeholder="Area" class="form-control" id="area' + (a) + '" name="area[]" required></td>';
            x += '<td><input type="text" placeholder="Kota" class="form-control" id="kota' + (a) + '" name="kota[]" required></td>';
            x += '<td><input type="text" placeholder="SOW" class="form-control" id="sow' + (a) + '" name="sow[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="tonase' + (a) + '" name="tonase[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="v_nilai' + (a) + '" name="v_nilai[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="v_deliv' + (a) + '" name="v_deliv[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="v_jasa' + (a) + '" name="v_jasa[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="g_nilai' + (a) + '" name="g_nilai[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="g_deliv' + (a) + '" name="g_deliv[]" required></td>';
            x += '<td><input type="number" min="1" placeholder="number" class="form-control" id="g_jasa' + (a) + '" name="g_jasa[]" required></td>';
            $("#multi_vendor tbody").append('<tr id="row' + (a) + '">' + x + "</tr>");
        });

        $(".removevendor").on("click", function() {
            if ($("#multi_vendor tbody tr").length > 1) {
                $("#multi_vendor tbody tr:last-child").remove();
                a--;
            } else {
                alert("Baris pertama isian tidak dapat dihapus")
            }
        });

        $("#cancelform").on("click", function() {
            $("#tabelpegawai").fadeIn('fast');
            $("#containerform").fadeOut();
            $("#formpegawai").html("")
        });

        $("#xfrm").on("submit", function(e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            }
            var link = 'order/simpanData/';
            var sData = new FormData($('#xfrm')[0]);
            $.ajax({
                url: link,
                type: "POST",
                data: sData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "html",

                beforeSend: function() {
                    $(".card #formpegawai").isLoading({
                        text: "Proses Simpan",
                        position: "overlay",
                        tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                    });
                },
                success: function(html) {
                    // setTimeout(function() {
                    //     $('#tabelpegawai').fadeIn('fast');
                    //     $('#containerform').fadeOut();
                    //     $('#formpegawai').html('');
                    //     location.reload(true);
                    // }, 1000);
                },
                error: function() {
                    setTimeout(function() {
                        $("#formpegawai").isLoading("hide");
                    }, 1000);
                }
            });
            return false;
        });
    }); /*]]>*/
</script>