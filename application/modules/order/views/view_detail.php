<div>
    <a href="javascript:;" id="back" class="btn btn-primary btn-md">
        <span class="m-0 font-weight-bold text-default text-center">BACK</span>
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Work Order</h6>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="col-label label-control">WO (Work Order)</label>
                    <div class="col-label">
                        <input type="text" class="form-control input-sm col-md-4" placeholder="Work Order" name="kode_wo" id="kode_wo" value="<?= $rowdata[0]->wo_mark ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-label label-control">Vendor</label>
                    <div class="col-label">
                        <input type="text" class="form-control input-sm col-md-4" placeholder="Vendor" value="<?= $rowdata[0]->vendor ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <table class="table" id="multi_vendor" style="width: 100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:20px">No</th>
                            <th>PRO ID</th>
                            <th>SITE</th>
                            <th>AREA</th>
                            <th>KOTA</th>
                            <th>SOW</th>
                            <th>TONASE</th>
                            <th colspan="3" style="text-align:center;" class="text-white btn-danger">PO VENDOR</th>
                            <th colspan="3" style="text-align:center;" class="text-white btn-success">PO GKN</th>
                            <th style="text-align:center;" class="text-white btn-info">Margin Vendor</th>
                        </tr>
                        <tr style="text-align:center;">
                            <th colspan="7"></th>
                            <th>Nilai Material</th>
                            <th>Delivery Material</th>
                            <th>Jasa Material</th>
                            <th>Nilai Material</th>
                            <th>Delivery Material</th>
                            <th>Jasa Material</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0;
                        foreach ($rowdata as $row) :
                            ?>
                            <tr id="row<?php echo ($no + 1); ?>">
                                <td style="text-align:center"><?php echo ($no + 1); ?></td>
                                <td><input type="text" placeholder="Project id" class="form-control" value="<?php echo $row->project_id; ?>" readonly></td>
                                <td><input type="text" placeholder="Site" class="form-control" value="<?php echo $row->site; ?>" readonly></td>
                                <td><input type="text" placeholder="Area" class="form-control" value="<?php echo $row->area; ?>" readonly></td>
                                <td><input type="text" placeholder="Kota" class="form-control" value="<?php echo $row->kota; ?>" readonly></td>
                                <td><input type="text" placeholder="SOW" class="form-control" value="<?php echo $row->sow; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->tonase; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->v_nilai; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->v_deliv; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->v_jasa; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->g_nilai; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->g_deliv; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo $row->g_jasa; ?>" readonly></td>
                                <td><input type="text" class="form-control" value="<?php echo "Rp " . number_format($row->total_pekerjaan_v - $row->total_pekerjaan_g, 2, ',', '.'); ?>" readonly></td>
                            </tr>
                        <?php
                            $no++;
                        endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                TOTAL
                            </td>
                            <td style="text-align:center;">
                                <?= $total[0]->totaltonase; ?>
                            </td>
                            <td colspan="3" style="text-align:center;">
                                <?= "Rp " . number_format($total[0]->totalvendor, 2, ',', '.'); ?>
                            </td>
                            <td colspan="3" style="text-align:center;">
                                <?= "Rp " . number_format($total[0]->totalgkn, 2, ',', '.'); ?>
                            </td>
                            <td colspan="3" style="text-align:center;">
                                <?= "Rp " . number_format($total[0]->totalmargin, 2, ',', '.'); ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Timeline</h6>
            </div>
            <div class="card-body">
                <ul class="timeline">
                    <li>
                        <a href="#">Marketing</a>
                        <a href="#" class="float-right"><?= (!is_null($timeline[0]->date_marketing)) ? date('d F Y', strtotime($timeline[0]->date_marketing)) : ''; ?></a>
                        <p><?= (!is_null($timeline[0]->date_marketing)) ? 'Done' : 'Not Done'; ?></p>
                    </li>
                    <li>
                        <a href="#">Budgeting</a>
                        <a href="#" class="float-right"><?= (!is_null($timeline[0]->date_budget)) ? date('d F Y', strtotime($timeline[0]->date_budget)) : ''; ?></a>
                        <p><?= (!is_null($timeline[0]->date_budget)) ? 'Done' : 'Not Done'; ?></p>
                    </li>
                    <li>
                        <a href="#">Operasional</a>
                        <a href="#" class="float-right"><?= (!is_null($timeline[0]->date_operasional)) ? date('d F Y', strtotime($timeline[0]->date_operasional)) : ''; ?></a>
                        <p><?= (!is_null($timeline[0]->date_operasional)) ? 'Done' : 'Not Done'; ?></p>
                    </li>
                    <li>
                        <a href="#">Administrasi</a>
                        <a href="#" class="float-right"><?= (!is_null($timeline[0]->date_admin)) ? date('d F Y', strtotime($timeline[0]->date_admin)) : ''; ?></a>
                        <p><?= (!is_null($timeline[0]->date_admin)) ? 'Done' : 'Not Done'; ?></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>

<script>
    $("#back").on("click", function() {
        $("#tabelpegawai").fadeIn('fast');
        $("#containerform").fadeOut();
        $("#contentdetail").html("");
    });

    $("#print").bind('click', function() {
        var jenis = $(this).data('jenis');
        var kode = $(this).data('kode');
        var link = "<?= base_url('download') ?>";
        $.ajax({
            url: link,
            type: "POST",
            data: "nama_jenis=" + jenis + "&kode=" + kode,
            dataType: "html",
            success: function(html) {

            }
        })
    })
</script>