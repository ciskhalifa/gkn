<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?= $this->uri->segment('1'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                                <div class="box-tools pull-right">
                                    <a href="javascript:;" class="btn btn-primary btn-sm" id="openform" data-tab="" data-original-title="Tambah Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="OrderTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row) : ?>
                                            <tr>
                                                <td><?= $row->wo_mark; ?></td>
                                                <td><?= $row->vendor; ?></td>
                                                <td><?= $row->site; ?></td>
                                                <td><?= $row->area; ?></td>
                                                <td><?= $row->kota; ?></td>
                                                <td><?= $row->sow; ?></td>
                                                <td>
                                                    <button type="button" data-kode="<?= $row->wo_mark ?>" class="btn btn-sm btn-outline-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                    <button type="button" data-kode="<?= $row->wo_mark ?>" class="btn btn-sm btn-outline-warning edit" id="edit"><i class="far fa-edit"></i></button>
                                                    <button type="button" data-kode="<?= $row->wo_mark ?>" class="btn btn-sm btn-outline-danger delete" id="delete"><i class="fa fa-trash"></i></button>
                                                    <a href="<?= (is_null($row->file_marketing)) ? '#' : base_url('publik/dok/' . $row->file_marketing) ?>" target="_BLANK" class="btn btn-sm btn-outline-danger openpdf" id="openpdf"><i class="fa fa-file-pdf"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div id="containerform" class="content-detached content-right animated--grow-in">
        <div class="content-body">
            <section class="row">
                <div class="col-md-12">
                    <div class="card shadow mb-2">
                        <form role="form" id="xfrm" class="form form-horizontal" enctype="multipart/form-data">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"><i class="icon-head"></i> Form Order</h4>
                            </div>
                            <div class="card-body">
                                <div id="formpegawai">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div id="containerdetail" class="content-detached content-right animated--grow-in">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left animated--grow-in" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal">
                    <!--<form id="sendForm" action="dosen/exportExcel" method="POST" enctype="multipart/form-data">-->
                    <div class="modal-header">
                        <h4 class="modal-title">Import Data</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input id="file" name="file" type="file" style="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Upload</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>