
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Order extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['content'] = 'order';
        $data['kolom'] = array("WO", "Vendor", "Nama Site", "Area", "Kota/Kab", "SOW", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing GROUP BY wo_mark", 3);
        $this->load->view('default', $data);
    }

    public function simpanData()
    {
        $arrdata = array();

        $arrdata['kode_wo'] = $this->input->post('kode_wo');
        $arrdata['user_data'] = $_SESSION['kode'];
        $arrdata['date_marketing'] = date('Y-m-d');
        $arrdata['kode_jenis'] = $this->input->post('kode_jenis');
        $kode =  str_replace('/', '', $this->input->post('kode_wo'));
        if (strlen(trim($_FILES['file_marketing']['name'])) > 0) {
            $files = $_FILES;
            $extenstion = pathinfo($_FILES['file_marketing']['name']);
            $ext = $extenstion['extension'];
            $_FILES['userfile']['name'] = "marketing_" . $kode . "_" . date("d-m-Y") . '.' . $ext;
            $_FILES['userfile']['type'] = $files['file_marketing']['type'];
            $_FILES['userfile']['tmp_name'] = $files['file_marketing']['tmp_name'];
            $_FILES['userfile']['error'] = $files['file_marketing']['error'];
            $_FILES['userfile']['size'] = $files['file_marketing']['size'];

            if (strlen(trim($_FILES['file_marketing']['name'])) > 0) {
                $basepath = BASEPATH;
                $stringreplace = str_replace("system", "publik", $basepath);
                $config['upload_path'] = $stringreplace . 'dok';
                $config['allowed_types'] = 'pdf|xls|xlsx|jpg|png';
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload()) {
                    $arrdata['file_marketing'] = "marketing_" . $kode . "_" . date("d-m-Y") . '.' . $ext;
                } else {
                    $error = array('error' => $this->upload->display_errors());
                }
            }
        }
        if ($this->input->post('cid') == '') {
            if ($this->input->post('vendor') && is_array($this->input->post('vendor'))) {
                for ($i = 0; $i < count($this->input->post('vendor')); $i++) {
                    $total_v = $this->input->post('v_nilai')[$i] + $this->input->post('v_deliv')[$i] + $this->input->post('v_jasa')[$i];
                    $total_pekerjaan_v = $total_v * $this->input->post('tonase')[$i];
                    $total_g = $this->input->post('g_nilai')[$i] + $this->input->post('g_deliv')[$i] + $this->input->post('g_jasa')[$i];
                    $total_pekerjaan_g = $total_g * $this->input->post('tonase')[$i];
                    $po_vendor['wo_mark'] = $this->input->post('kode_wo');
                    $po_vendor['vendor'] = $this->input->post('vendor')[$i];
                    $po_vendor['project_id'] = $this->input->post('project_id')[$i];
                    $po_vendor['site'] = $this->input->post('site')[$i];
                    $po_vendor['area'] = $this->input->post('area')[$i];
                    $po_vendor['kota'] = $this->input->post('kota')[$i];
                    $po_vendor['sow'] = $this->input->post('sow')[$i];
                    $po_vendor['tonase'] = $this->input->post('tonase')[$i];
                    $po_vendor['v_nilai'] = $this->input->post('v_nilai')[$i];
                    $po_vendor['v_deliv'] = $this->input->post('v_deliv')[$i];
                    $po_vendor['v_jasa'] = $this->input->post('v_jasa')[$i];
                    $po_vendor['g_nilai'] = $this->input->post('g_nilai')[$i];
                    $po_vendor['g_deliv'] = $this->input->post('g_deliv')[$i];
                    $po_vendor['g_jasa'] = $this->input->post('g_jasa')[$i];
                    $po_vendor['total_v'] = $total_v;
                    $po_vendor['total_pekerjaan_v'] = $total_pekerjaan_v;
                    $po_vendor['total_g'] = $total_g;
                    $po_vendor['total_pekerjaan_g'] = $total_pekerjaan_g;
                    $this->Data_model->simpanData($po_vendor, 't_detail');
                }
            }
            $this->Data_model->simpanData($arrdata, 't_order');
        } else {
            if ($this->input->post('vendor') && is_array($this->input->post('vendor'))) {
                $q = "DELETE FROM t_detail WHERE wo_mark IN ('" . $this->input->post('kode_wo') . "')";
                $e = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing WHERE wo_mark='" . $this->input->post('kode_wo') . "'", 3);
                $this->Data_model->jalankanQuery($q);
                for ($i = 0; $i < count($this->input->post('vendor')); $i++) {
                    $total_v = $this->input->post('v_nilai')[$i] + $this->input->post('v_deliv')[$i] + $this->input->post('v_jasa')[$i];
                    $total_pekerjaan_v = $total_v * $this->input->post('tonase')[$i];
                    $total_g = $this->input->post('g_nilai')[$i] + $this->input->post('g_deliv')[$i] + $this->input->post('g_jasa')[$i];
                    $total_pekerjaan_g = $total_g * $this->input->post('tonase')[$i];
                    $total_e = $e[$i]->e_nilai + $e[$i]->e_deliv + $e[$i]->e_jasa;
                    $total_pekerjaan_e = $total_e * $this->input->post('tonase')[$i];
                    $po_vendor['wo_mark'] = $this->input->post('kode_wo');
                    $po_vendor['vendor'] = $this->input->post('vendor')[$i];
                    $po_vendor['project_id'] = $this->input->post('project_id')[$i];
                    $po_vendor['site'] = $this->input->post('site')[$i];
                    $po_vendor['area'] = $this->input->post('area')[$i];
                    $po_vendor['kota'] = $this->input->post('kota')[$i];
                    $po_vendor['sow'] = $this->input->post('sow')[$i];
                    $po_vendor['tonase'] = $this->input->post('tonase')[$i];
                    $po_vendor['v_nilai'] = $this->input->post('v_nilai')[$i];
                    $po_vendor['v_deliv'] = $this->input->post('v_deliv')[$i];
                    $po_vendor['v_jasa'] = $this->input->post('v_jasa')[$i];
                    $po_vendor['g_nilai'] = $this->input->post('g_nilai')[$i];
                    $po_vendor['g_deliv'] = $this->input->post('g_deliv')[$i];
                    $po_vendor['g_jasa'] = $this->input->post('g_jasa')[$i];
                    $po_vendor['e_nilai'] = $e[$i]->e_nilai;
                    $po_vendor['e_deliv'] = $e[$i]->e_deliv;
                    $po_vendor['e_jasa'] = $e[$i]->e_jasa;
                    $po_vendor['total_e'] = $total_e;
                    $po_vendor['total_pekerjaan_e'] = $total_pekerjaan_e;
                    $po_vendor['total_v'] = $total_v;
                    $po_vendor['total_pekerjaan_v'] = $total_pekerjaan_v;
                    $po_vendor['total_g'] = $total_g;
                    $po_vendor['total_pekerjaan_g'] = $total_pekerjaan_g;
                    $this->Data_model->simpanData($po_vendor, 't_detail');
                }
            }
            $this->Data_model->updateDataWhere($arrdata, 't_order', array('kode_wo' => $this->input->post('cid')));
        }
    }

    public function form()
    {

        if ($this->input->post('kode_wo') != '') {
            $halaman = $this->uri->segment(3);
            $nilai = $this->input->post('kode_wo');
            $kondisi = 'wo_mark';
            $konten['aep'] = '';
            $konten['rowdata'] = $this->Data_model->satuData('v_marketing', array($kondisi => $nilai));
            $konten['rowvendor'] = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing WHERE wo_mark ='$nilai'", 3);
        } else {
            $halaman = $this->uri->segment(3);
            $konten['aep'] = $this->uri->segment(3);
        }
        $this->load->view('form_' . $halaman, $konten);
    }

    public function view_detail()
    {
        $id = $this->input->post('kode_wo');
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing WHERE wo_mark='$id'", 3);
        $data['timeline'] = $this->Data_model->jalankanQuery("SELECT * FROM t_order WHERE kode_wo='$id'", 3);
        $data['total'] = $this->Data_model->jalankanQuery("SELECT SUM(tonase) AS totaltonase, SUM(total_pekerjaan_v) AS totalvendor, SUM(total_pekerjaan_g) AS totalgkn, SUM(total_pekerjaan_v - total_pekerjaan_g) as totalmargin  FROM v_marketing WHERE wo_mark='$id'", 3);
        $this->load->view('view_detail', $data);
    }

    public function getKode()
    {
        // $format = "GKN/URUTAN/JENIS/BULAN/TAHUN";
        $huruf = "GKN";
        $jenis = $this->input->post('jenis');
        $bulan = date('m');
        $tahun = date('Y');
        $q = $this->Data_model->jalankanQuery("SELECT kode_wo FROM t_order ORDER BY kode_wo DESC LIMIT 1", 3);
        $y = $this->Data_model->jalankanQuery("SELECT count(kode_jenis) as total FROM t_order WHERE kode_jenis=" . $this->input->post('kode_jenis'), 3);
        $pecah = explode('/', $q[0]->kode_wo);
        if (date('d') == '01' || $y[0]->total == 0) {
            $x = '01';
        } else if ($y[0]->total != 0) {
            $x = sprintf("%02s", $pecah[1] + 1);
        }

        $no_wo = $huruf . '/' . $x . '/' . $jenis . '/' . $bulan . '/' . $tahun;
        echo $no_wo;
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode_wo' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode_wo' => $param);
                }
                $this->Data_model->hapusDataWhere('t_order', $kondisi);
                $this->Data_model->hapusDataWhere('t_detail', array('wo_mark' => $param));
                echo json_encode("ok");
            }
        }
    }
}
