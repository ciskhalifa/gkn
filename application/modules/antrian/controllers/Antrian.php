
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Antrian extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'antrian';
        if ($_SESSION['role'] == '1') {
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vantrian WHERE agent=" . $_SESSION['kode'], 3);
        } else if ($_SESSION['role'] == '2') {
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vantrian WHERE ch=" . $_SESSION['kode'] . " OR ch IS NULL", 3);
        }
        $data['kolom'] = array("Kode", "Pemesan", "Telepon", "Daerah", "SPBU", "Urutan", "Status", "Opsi");
        $q = $this->Data_model->jalankanQuery("SELECT max(kode_antri) as maxKode FROM t_antrian", 3);
        $noUrut = (int) substr($q[0]->maxKode, 3, 3);
        $noUrut++;
        $char = "ANT";
        $data['no_antrian'] = $char . sprintf("%03s", $noUrut);
        $this->load->view('default', $data);
    }

    public function simpanData()
    {
        $daerah = $this->input->post('kecamatan');
        $arrdata = array();
        $pesanan = array();

        // CEK QUOTA SPBU
        $cek = $this->cekQuotaSPBU($daerah);
        if ($cek['return'] == '1') {
            $arrdata['kode_spbu'] = $cek['kode_spbu'];
            $arrdata['status'] = $cek['status'];
            $arrdata['urutan'] = $cek['urutan'];
        } else if ($cek['return'] == '2') {
            $arrdata['kode_spbu'] = $cek['kode_spbu'];
            $arrdata['status'] = $cek['status'];
            $arrdata['urutan'] = $cek['urutan'];
        } else {
            $arrdata['kode_spbu'] = $cek['kode_spbu'];
            $arrdata['status'] = $cek['status'];
            $arrdata['urutan'] = $cek['urutan'];
        }


        $arrdata['kode_antri'] = $this->input->post('no_antrian');
        $arrdata['nama_pemesan'] = $this->input->post('nama_pemesan');
        $arrdata['no_hp'] = $this->input->post('no_hp');
        $arrdata['no_lainnya'] = $this->input->post('no_lainnya');
        $arrdata['alamat'] = $this->input->post('alamat');
        $arrdata['agent'] = $_SESSION['kode'];
        $arrdata['kode_daerah'] = $daerah;


        if ($this->input->post('jenis') && is_array($this->input->post('jenis'))) {
            for ($i = 0; $i < count($this->input->post('jenis')); $i++) {
                $q = $this->Data_model->jalankanQuery("SELECT * FROM m_jenis WHERE kode=" . $this->input->post('barang')[$i], 3);
                $pesanan['kode'] = $arrdata['kode_antri'];
                $pesanan['jenis'] = $this->input->post('barang')[$i];
                $pesanan['jumlah'] = $this->input->post('jumlah')[$i];
                $pesanan['subtotal'] = $q[0]->harga * $this->input->post('jumlah')[$i];
                $this->Data_model->simpanData($pesanan, 't_antrian_detail');
            }
        }
        $this->Data_model->simpanData($arrdata, 't_antrian');
        $this->updateQuota($arrdata['kode_spbu']);
        redirect('antrian');
    }
    public function updateStatus()
    {
        $arrdata['status'] = $this->input->post('status');
        if ($this->input->post('status') == 'ON PROGRESS') {
            $arrdata['lup_oprogress'] = date('Y-m-d H:i:s');
        } else if ($this->input->post('status') == 'SUCCESS') {
            $arrdata['lup_osuccess'] = date('Y-m-d H:i:s');
        } else if ($this->input->post('status') == 'CANCEL') {
            $arrdata['lup_ocancel'] = date('Y-m-d H:i:s');
        } else {
            $arrdata['lup_oantri'] = date('Y-m-d H:i:s');
        }
        $arrdata['ch'] = $_SESSION['kode'];
        $arrdata['lup_ch'] = date('Y-m-d H:i:s');
        $this->Data_model->updateDataWhere($arrdata, 't_antrian', array('kode_antri' => $this->input->post('kode')));
        echo json_encode("ok");
    }
    public function updateQuota($spbu)
    {
        $stok = $this->Data_model->jalankanQuery("SELECT * FROM m_spbu WHERE kode=" . $spbu, 3);
        $stokbaru = $stok[0]->quota - 1;
        $this->Data_model->updateDataWhere(array('quota' => $stokbaru), 'm_spbu', array('kode' => $spbu));
    }
    public function resetQuota()
    {
        $this->Data_model->jalankanQuery("UPDATE m_spbu set quota = 4", 3);
    }
    public function cekQuotaSPBU($kode)
    {

        $data = array();
        $q = $this->Data_model->jalankanQuery("SELECT * FROM combospbu WHERE kode=$kode", 3);
        $res1 = $this->Data_model->jalankanQuery("SELECT MAX(urutan) as jumlah FROM t_antrian WHERE DATE(tgl_data) = DATE(NOW()) AND kode_spbu=" . $q[0]->kode_spbu1 . " AND kode_daerah=" . $kode, 3);
        $res2 = $this->Data_model->jalankanQuery("SELECT MAX(urutan) as jumlah FROM t_antrian WHERE DATE(tgl_data) = DATE(NOW()) AND kode_spbu=" . $q[0]->kode_spbu2 . " AND kode_daerah=" . $kode, 3);
        if ($q[0]->q1 > 0) {
            if ($res1[0]->jumlah == "" && date('Hi') <= "1400") {
                $data['urutan'] = 0 + 1;
                $data['status'] = 'ANTRI';
            } else {
                if ($res1[0]->jumlah >= 4 && date('Hi') >= "1400") {
                    $data['urutan'] = 0 + 1;
                    $data['status'] = 'BESOK';
                } else if ($res1[0]->jumlah <= 4 && date('Hi') <= "1400") {
                    $data['urutan'] = $res1[0]->jumlah + 1;
                    $data['status'] = 'ANTRI';
                } else {
                    $data['urutan'] = $res1[0]->jumlah + 1;
                    $data['status'] = 'BESOK';
                }
            }
            $data['kode_spbu'] = $q[0]->kode_spbu1;
            $data['return'] = 1;
            return $data;
        } else if ($q[0]->q2 > 0) {
            if ($res2[0]->jumlah == "" && date('Hi') <= "1400") {
                $data['urutan'] = 0 + 1;
                $data['status'] = 'ANTRI';
            } else {
                if ($res2[0]->jumlah >= 4 && date('Hi') >= "1400") {
                    $data['urutan'] = 0 + 1;
                    $data['status'] = 'BESOK';
                } else if ($res2[0]->jumlah <= 4 && date('Hi') <= "1400") {
                    $data['urutan'] = $res2[0]->jumlah + 1;
                    $data['status'] = 'ANTRI';
                } else {
                    $data['urutan'] = $res2[0]->jumlah + 1;
                    $data['status'] = 'BESOK';
                }
            }
            $data['kode_spbu'] = $q[0]->kode_spbu2;
            $data['return'] = 2;
            return $data;
        } else {
            $data['kode_spbu'] = $q[0]->kode_spbu1;
            $data['urutan'] = 1;
            $data['status'] = 'BESOK';
            $data['return'] = 3;
            return $data;
        }
    }
    public function getSPBU()
    {
        $kabupaten = $this->Data_model->ambilDataWhere('combospbu', array('kode' => $_GET['kode']), 'spbu1', 'asc');
        echo '<select class="select2 form-control spbu" name="spbu" id="spbu" style="width:100%">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kabupaten as $row) {
            echo '<option data-spbu2= "' . $row->spbu2 . '" data-spbu1="' . $row->spbu1 . '" value="' . $row->kode . '">' . $row->spbu1 . ' (' . $row->q1 . ') - ' . $row->spbu2 . ' (' . $row->q2 . ')' . '</option>';
        }
        echo '</select>';
    }
    public function select_jenis()
    {
        $query = "SELECT DISTINCT(tipe) AS disp FROM m_jenis";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }
    public function getBarang()
    {
        $get = $_GET['tipe'];
        $query = "SELECT kode,nama_jenis AS disp FROM m_jenis WHERE tipe='$get'";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }

    public function view_detail()
    {
        $id = $this->uri->segment(3);
        $kondisi = "kode_antri";
        $data['rowdata'] = $this->Data_model->satuData('vantrian', array($kondisi => $id));
        $data['rowdetail'] = $this->Data_model->jalankanQuery("SELECT t_antrian_detail.*, m_jenis.nama_jenis FROM  t_antrian_detail JOIN m_jenis ON m_jenis.kode = t_antrian_detail.jenis WHERE t_antrian_detail.kode='$id'", 3);
        $data['total'] = $this->Data_model->jalankanQuery("SELECT SUM(subtotal) as total FROM t_antrian_detail WHERE kode='$id'", 3);
        $this->load->view('view_detail', $data);
    }
    public function getList()
    {
        $tabel = $this->input->post('tabel');
        $param = $this->input->post('param');
        $field = $this->input->post('fld');
        if (strpos($this->input->post('kolom'), ',')) {
            $arrnmfld = explode(',', $this->input->post('kolom'));
            $nmfld1 = $arrnmfld[0];
            $nmfld2 = $arrnmfld[1];
            $grpby = array($nmfld1, $nmfld2);
        } else {
            $nmfld1 = $this->input->post('kolom');
            $nmfld2 = $this->input->post('kolom');
            $grpby = array($nmfld1);
        }
        if (strpos($this->input->post('fld'), ',')) {
            $f = explode(",", $field);
            $p = explode("-", $param);
            $kondisi = array($f[0] => $p[0], $f[1] => $p[1]);
        } else {
            $kondisi = array($field => $param);
        }
        $order = "1";
        $query = $this->db->select("$nmfld1,$nmfld2")->order_by($order, 'ASC')->group_by($grpby)->get_where($tabel, $kondisi);
        $result = $query->result();
        $data = array();
        if (!empty($result)) {
            foreach ($result as $row) {
                $data[] = '{"id" : "' . $row->$nmfld1 . '", "value" : "' . trim($row->$nmfld2) . '", "nomor": "' . $row->$nmfld1 . '", "sub":0, "isigrup":0}';
            }
        }
        if (IS_AJAX) {
            echo '[' . implode(',', $data) . ']';
        }
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode_antri' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode_antri' => $param);
                }
                $this->Data_model->hapusDataWhere('t_antrian', $kondisi);
                $this->Data_model->hapusDataWhere('t_antrian_detail', array('kode' => $param));
                echo json_encode("ok");
            }
        }
    }
}
