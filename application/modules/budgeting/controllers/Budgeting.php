
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Budgeting extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['content'] = 'budgeting';
        $data['kolom'] = array("WO", "Vendor", "Nama Site", "Area", "Kota/Kab", "SOW", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing GROUP BY wo_mark", 3);
        $this->load->view('default', $data);
    }

    public function simpanData()
    {
        $arrdata = array();

        $arrdata['kode_wo'] = $this->input->post('kode_wo');
        $arrdata['date_budget'] = date('Y-m-d');
        $kode =  str_replace('/', '', $this->input->post('kode_wo'));
        if (strlen(trim($_FILES['file_budgeting']['name'])) > 0) {
            $files = $_FILES;
            $extenstion = pathinfo($_FILES['file_budgeting']['name']);
            $ext = $extenstion['extension'];
            $_FILES['userfile']['name'] = "budgeting_" . $kode . "_" . date("d-m-Y") . '.' . $ext;
            $_FILES['userfile']['type'] = $files['file_budgeting']['type'];
            $_FILES['userfile']['tmp_name'] = $files['file_budgeting']['tmp_name'];
            $_FILES['userfile']['error'] = $files['file_budgeting']['error'];
            $_FILES['userfile']['size'] = $files['file_budgeting']['size'];

            if (strlen(trim($_FILES['file_budgeting']['name'])) > 0) {
                $basepath = BASEPATH;
                $stringreplace = str_replace("system", "publik", $basepath);
                $config['upload_path'] = $stringreplace . 'dok';
                $config['allowed_types'] = 'pdf|xls|xlsx|jpg|png';
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload()) {
                    $arrdata['file_budgeting'] = "budgeting_" . $kode . "_" . date("d-m-Y") . '.' . $ext;
                } else {
                    $error = array('error' => $this->upload->display_errors());
                }
            }
        }
        if ($this->input->post('e_nilai') && is_array($this->input->post('e_nilai'))) {
            for ($i = 0; $i < count($this->input->post('vendor')); $i++) {
                $total_e = $this->input->post('e_nilai')[$i] + $this->input->post('e_deliv')[$i] + $this->input->post('e_jasa')[$i];
                $total_pekerjaan_e = $total_e * $this->input->post('tonase')[$i];
                $po_vendor['e_nilai'] = $this->input->post('e_nilai')[$i];
                $po_vendor['e_deliv'] = $this->input->post('e_deliv')[$i];
                $po_vendor['e_jasa'] = $this->input->post('e_jasa')[$i];
                $po_vendor['total_e'] = $total_e;
                $po_vendor['total_pekerjaan_e'] = $total_pekerjaan_e;
                $this->Data_model->updateDataWhere($po_vendor, 't_detail', array('wo_mark' => $this->input->post('cid')));
            }
        }
        $this->Data_model->updateDataWhere($arrdata, 't_order', array('kode_wo' => $this->input->post('cid')));
    }

    public function form()
    {

        if ($this->input->post('kode_wo') != '') {
            $halaman = $this->uri->segment(3);
            $nilai = $this->input->post('kode_wo');
            $kondisi = 'wo_mark';
            $konten['aep'] = '';
            $konten['rowdata'] = $this->Data_model->satuData('v_marketing', array($kondisi => $nilai));
            $konten['rowvendor'] = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing WHERE wo_mark ='$nilai'", 3);
        } else {
            $halaman = $this->uri->segment(3);
            $konten['aep'] = $this->uri->segment(3);
        }
        $this->load->view('form_' . $halaman, $konten);
    }

    public function view_detail()
    {
        $id = $this->input->post('kode_wo');
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM v_marketing WHERE wo_mark='$id'", 3);
        $data['timeline'] = $this->Data_model->jalankanQuery("SELECT * FROM t_order WHERE kode_wo='$id'", 3);
        $data['total'] = $this->Data_model->jalankanQuery("SELECT SUM(tonase) AS totaltonase, SUM(total_pekerjaan_e) AS totalestimasi, SUM(total_pekerjaan_g) AS totalgkn  FROM v_marketing WHERE wo_mark='$id'", 3);
        $this->load->view('view_detail', $data);
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode_wo' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode_wo' => $param);
                }
                $this->Data_model->hapusDataWhere('t_order', $kondisi);
                $this->Data_model->hapusDataWhere('t_detail', array('wo_mark' => $param));
                echo json_encode("ok");
            }
        }
    }

    public function openPDF($filename)
    {
        
        // Header content type 
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');

        // Read the file 
        @readfile($filename);
    }
}
