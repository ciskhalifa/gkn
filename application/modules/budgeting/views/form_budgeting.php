<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai) :
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = ($aep == 'salin') ? '' : $arey['wo_mark'];
} else {
    $cid = '';
}

?>
<div class="container-fluid">
    <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-label label-control">WO (Work Order)</label>
                <div class="col-label">
                    <input type="text" class="form-control input-sm col-md-4" placeholder="Work Order" name="kode_wo" id="kode_wo" value="<?= (isset($arey)) ? $arey['wo_mark'] : ''; ?>" data-error="wajib diisi" readonly>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group" id="po_vendor">
                <div class="col-label">
                    <table class="table" id="multi_vendor" style="width: 100%">
                        <thead>
                            <tr>
                                <th style="text-align:center;width:20px">No</th>
                                <th>VENDOR</th>
                                <th>PROJECT ID</th>
                                <th>SITE</th>
                                <th>AREA</th>
                                <th>KOTA</th>
                                <th>SOW</th>
                                <th>TONASE</th>
                                <th colspan="3" style="text-align:center;" class="text-white btn-danger">ESTIMASI NILAI AKTUAL</th>
                            </tr>
                            <tr>
                                <th colspan="8"></th>
                                <th>Nilai Material</th>
                                <th>Delivery Material</th>
                                <th>Jasa Material</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $no = 0;
                            foreach ($rowvendor as $row) :
                                ?>
                                <tr id="row<?php echo ($no + 1); ?>">
                                    <td style="text-align:center"><?php echo ($no + 1); ?></td>
                                    <td><input type="text" class="form-control" id="vendor<?php echo ($no + 1); ?>" name="vendor[]" value="<?php echo $row->vendor; ?>" readonly></td>
                                    <td><input type="text" class="form-control" id="project_id<?php echo ($no + 1); ?>" name="project_id[]" value="<?php echo $row->project_id; ?>" readonly></td>
                                    <td><input type="text" class="form-control" id="site<?php echo ($no + 1); ?>" name="site[]" value="<?php echo $row->site; ?>" readonly></td>
                                    <td><input type="text" class="form-control" id="area<?php echo ($no + 1); ?>" name="area[]" value="<?php echo $row->area; ?>" readonly></td>
                                    <td><input type="text" class="form-control" id="kota<?php echo ($no + 1); ?>" name="kota[]" value="<?php echo $row->kota; ?>" readonly></td>
                                    <td><input type="text" class="form-control" id="sow<?php echo ($no + 1); ?>" name="sow[]" value="<?php echo $row->sow; ?>" readonly></td>
                                    <td><input type="number" min="1" class="form-control" id="tonase<?php echo ($no + 1); ?>" name="tonase[]" value="<?php echo $row->tonase; ?>" readonly></td>
                                    <td><input type="number" min="1" class="form-control" id="e_nilai<?php echo ($no + 1); ?>" name="e_nilai[]" value="<?php echo $row->e_nilai; ?>" required></td>
                                    <td><input type="number" min="1" class="form-control" id="e_deliv<?php echo ($no + 1); ?>" name="e_deliv[]" value="<?php echo $row->e_deliv; ?>" required></td>
                                    <td><input type="number" min="1" class="form-control" id="e_jasa<?php echo ($no + 1); ?>" name="e_jasa[]" value="<?php echo $row->e_jasa; ?>" required></td>
                                </tr>
                            <?php
                                $no++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-label label-control">Dokumen</label>
        <div class="col-label">
            <input type="file" accept="application/pdf" class="form-control input-sm col-md-4" placeholder="Work Order" name="file_budgeting" id="file" value="<?= (isset($arey)) ? $arey['file_budgeting'] : ''; ?>" data-error="wajib diisi" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" id="saveform" class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
        <button type="button" id="cancelform" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $("#cancelform").on("click", function() {
            $("#tabelpegawai").fadeIn('fast');
            $("#containerform").fadeOut();
            $("#formpegawai").html("")
        });

        $("#xfrm").on("submit", function(e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            }
            var link = 'budgeting/simpanData/';
            var sData = new FormData($('#xfrm')[0]);
            $.ajax({
                url: link,
                type: "POST",
                data: sData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "html",

                beforeSend: function() {
                    $(".card #formpegawai").isLoading({
                        text: "Proses Simpan",
                        position: "overlay",
                        tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                    });
                },
                success: function(html) {
                    setTimeout(function() {
                        $('#tabelpegawai').fadeIn('fast');
                        $('#containerform').fadeOut();
                        $('#formpegawai').html('');
                        // location.reload(true);
                    }, 1000);
                },
                error: function() {
                    setTimeout(function() {
                        $("#formpegawai").isLoading("hide");
                    }, 1000);
                }
            });
            return false;
        });
    }); /*]]>*/
</script>