
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        if ($_SESSION['role'] == '3') {
            // OPERASIONAL
            $data['js'] = 'js_operasional';
            $data['css'] = 'css_operasional';
            $data['content'] = 'dashboard_operasional';
        } else if ($_SESSION['role'] == '2') {
            // MARKETING
            $data['js'] = 'js_marketing';
            $data['css'] = 'css_marketing';
            $data['content'] = 'dashboard_marketing';
        } else if ($_SESSION['role'] == '1') {
            // ADMIN
            $data['js'] = 'js_admin';
            $data['css'] = 'css_admin';
            $data['content'] = 'dashboard_admin';
        } else if ($_SESSION['role'] == '4'){
            // BUDGETING
            $data['js'] = 'js_budgeting';
            $data['css'] = 'css_budgeting';
            $data['content'] = 'dashboard_budgeting';
        } else if ($_SESSION['role'] == '6') {
            // KEUANGAN
            $data['js'] = "js_keuangan";
            $data['css'] = 'css_keuangan';
            $data['content'] = 'dashboard_keuangan';
        }
        $this->load->view('default', $data);
    }
}
