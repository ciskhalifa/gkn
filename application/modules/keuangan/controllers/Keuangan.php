
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Keuangan extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['content'] = 'keuangan';
        $data['kolom'] = array("Tanggal", "Users", "Tanggal Data", "Opsi");
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM v_keuangan GROUP BY month(tanggal)", 3);
        $this->load->view('default', $data);
    }

    public function simpanData()
    {
        $arrdata = array();
        if ($this->input->post('cid') == '') {
            $arrdata['user_data'] = $_SESSION['kode'];
            $arrdata['tanggal'] = date('Y-m-d', strtotime($this->input->post('tanggal')));
            $arrdata['uraian'] = $this->input->post('uraian');
            if ($this->input->post('remarks') == "CR") {
                $arrdata['remarks'] = $this->input->post('remarks');
                $arrdata['kredit'] = $this->input->post('nominal');
                $arrdata['saldo'] = $this->input->post('saldo');
            } else {
                $arrdata['remarks'] = $this->input->post('remarks');
                $arrdata['debet'] = $this->input->post('nominal');
                $arrdata['saldo'] = $this->input->post('saldo');
            }
            $this->Data_model->simpanData($arrdata, 't_keuangan');
        } else {
            $arrdata['user_data'] = $_SESSION['kode'];
            $arrdata['tanggal'] = date('Y-m-d', strtotime($this->input->post('tanggal')));
            $arrdata['uraian'] = $this->input->post('uraian');
            if ($this->input->post('remarks') == "CR") {
                $arrdata['remarks'] = $this->input->post('remarks');
                $arrdata['kredit'] = $this->input->post('nominal');
                $arrdata['saldo'] = $this->input->post('saldo');
            } else {
                $arrdata['remarks'] = $this->input->post('remarks');
                $arrdata['debet'] = $this->input->post('nominal');
                $arrdata['saldo'] = $this->input->post('saldo');
            }
            $this->Data_model->updateDataWhere($arrdata, 't_keuangan', array('kode' => $this->input->post('cid')));
        }
        redirect('keuangan');
    }

    public function view_detail()
    {
        $id = $this->uri->segment(3);
        $kondisi = "wo_mark";
        $data['rowdata'] = $this->Data_model->satuData('v_marketing', array($kondisi => $id));
        $this->load->view('view_detail', $data);
    }

    public function getSaldo()
    {
        $saldo = $this->Data_model->jalankanQuery("SELECT * FROM t_keuangan ORDER BY kode DESC LIMIT 1", 3);

        if (empty($saldo[0]->saldo)) {
            if ($this->input->post('jenis') == 'CR') {
                $aldo = 0 + $this->input->post('nominal');
            } else {
                $aldo = $this->input->post('nominal');
            }
        } else {
            if ($this->input->post('jenis') == 'CR') {
                $aldo = $saldo[0]->saldo + $this->input->post('nominal');
            } else {
                $aldo = $saldo[0]->saldo - $this->input->post('nominal');
            }
        }
        echo $aldo;
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('t_keuangan', $kondisi);
                echo json_encode("ok");
            }
        }
    }
}
