<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>

<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan menghapus data <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    $(function() {
        $("#OrderTable").dataTable();
        $(".nominal").on('keyup', function() {
            $.ajax({
                url: "<?= base_url('keuangan/getSaldo'); ?>",
                type: "POST",
                data: "jenis=" + $('#remarks').find(':selected').val() + "&nominal=" + $(this).val(),
                dataType: "html",
                success: function(html) {
                    $('.saldo').val(formatRupiah(html, 'Rp. '));
                    $('.saldoakhir').val(html);
                }
            });

        })
        $('#openform').on('click', function() {
            $('#modalForm').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $('#modalForm').on('hidden.bs.modal', function() {
            $(this).find('form')[0].reset();
            if ($("#pesanan tbody tr").length > 1) {
                $("#pesanan tbody tr:last-child").remove();
            }
        });
        $('#import').on('click', function() {
            $('#modalImport').modal();
        });
        $('#report').on('click', function() {
            $('#modalExport').modal();
        });
        $("#file").on("change", function() {
            var file = this.files[0];
            var fileName = file.name;
            var fileType = file.type;
            console.log(fileType);
            if (fileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || fileType == "application/vnd.ms-excel") {
                $('#label').text(fileName);
                $('#reset').removeClass('hidden');
            } else {
                alert("Maaf format dokumen tidak sesuai");
                $(this).val('');
                $('#label').text('Pilih File');
            }
        });

        $(".detail").bind('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'budgeting/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });

        $('.edit').bind("click", function() {
            var link = "<?= base_url('budgeting/form/budgeting') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode_wo=" + $(this).attr("data-kode"),
                dataType: "html",
                beforeSend: function() {
                    if (link != "#") {}
                },
                success: function(html) {
                    $('#tabelpegawai').hide();
                    $('#formpegawai').html(html);
                    $('#containerform').fadeIn('fast');
                }
            })
        });

        $(".delete").on('click', function() {
            $("#myConfirm").modal();
            $(".lblModal").text($(this).data('kode'));
            $("#cid").val($(this).data('kode'));
            $("#getto").val("<?= base_url('budgeting/hapus') ?>");
        });

        $("#btnYes").bind("click", function() {
            var link = $("#getto").val();
            $.ajax({
                url: link,
                type: "POST",
                data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(),
                dataType: "html",
                beforeSend: function() {
                    if (link != "#") {}
                },
                success: function(html) {
                    location.reload(true);
                    // myApp.oTable.fnDraw(false);
                    $("#myConfirm").modal("hide")
                }
            })
        });
    });
</script>