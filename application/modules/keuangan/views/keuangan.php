<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?= $this->uri->segment('1'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                                <div class="box-tools pull-right">
                                    <a href="javascript:;" class="btn btn-primary btn-sm" id="openform" data-tab="" data-original-title="Tambah Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="OrderTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row) : ?>
                                            <tr>
                                                <td><?= date('F Y', strtotime($row->tanggal)); ?></td>
                                                <td><?= $row->nama_lengkap; ?></td>
                                                <td><?= date('d F Y', strtotime($row->tgl_data)); ?></td>
                                                <td>
                                                    <button type="button" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-outline-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                    <button type="button" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-outline-warning edit" id="edit"><i class="far fa-edit"></i></button>
                                                    <button type="button" data-kode="<?= $row->kode ?>" class="btn btn-sm btn-outline-danger delete" id="delete"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div id="containerdetail" class="content-detached">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left animated--grow-in" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal" action="<?= base_url('keuangan/simpanData'); ?>" method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Keuangan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tanggal</label>
                            <div class="col-md-8">
                                <input type="date" class="form-control input-sm" placeholder="Tanggal" name="tanggal" id="tanggal" value="" data-error="wajib diisi" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Remarks</label>
                            <div class="col-md-8">
                                <select class="select2 form-control" name="remarks" id="remarks" style="width:100%">
                                    <option value="">- Pilihan -</option>
                                    <option value="DB">DEBET</option>
                                    <option value="CR">KREDIT</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Uraian</label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" placeholder="Uraian" name="uraian" id="uraian" value=""></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Nominal</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm nominal" placeholder="Nominal" name="nominal" id="nominal" value="" data-error="wajib diisi" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Saldo</label>
                            <div class="col-md-8">
                                <input type="hidden" class="form-control input-sm saldoakhir" placeholder="Saldo" name="saldo" id="nama_pemesan" value="" data-error="wajib diisi" required readonly>
                                <input type="text" class="form-control input-sm saldo" placeholder="Saldo" value="" data-error="wajib diisi" required readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>