<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $pekerjaan = $rowdata->pekerjaan;
    $tonase = $rowdata->tonase;
    $harga_satuan = $rowdata->harga_satuan;
} else {
    $cid = "";
    $pekerjaan = "";
    $tonase = "";
    $harga_satuan = "";
}
?>

<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal" method="POST">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Nama Pekerjaan</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Nama Pekerjaan" name="pekerjaan" id="pekerjaan" value="<?php echo $pekerjaan; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Tonase</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Tonase" name="tonase" id="tonase" value="<?php echo $tonase; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Harga Satuan</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Harga Satuan" name="harga_satuan" id="harga_satuan" value="<?php echo $harga_satuan; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function(c) {
            if (c.isDefaultPrevented()) {} else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = new FormData($('#xfrm')[0]);
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //dataType: "html",
                    beforeSend: function() {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: ''
                        })
                    },
                    success: function(d) {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function() {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>