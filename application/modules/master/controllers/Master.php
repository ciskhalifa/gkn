<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Master extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        // halaman
        $data['content'] = 'master';
        // js tambahan dan css
        $data['js'] = 'js';
        $data['css'] = 'css';

        $this->load->view('default', $data);
    }

    public function loadHalaman()
    {
        $konten['tabel'] = $this->uri->segment(3); // barang
        switch ($this->uri->segment(3)):
            case 'pekerjaan':
                $konten['kolom'] = array("Kode", "Pekerjaan", "Tonase/Kg", "Harga Satuan", "Opsi");
                break;
            case 'vendor':
                $konten['kolom'] = array("Kode", "Vendor", "Opsi");
                break;
            case 'jenis':
                $konten['kolom'] = array("Kode", "Nama Jenis", "Singkatan", "Opsi");
                break;
            case 'user':
                $konten['kolom'] = array("Kode", "Nama Lengkap", "Username", "Role", "Opsi");
                break;
            default:
                $konten['kolom'] = "";
                break;
        endswitch;

        $konten['jmlkolom'] = count($konten['kolom']);
        $this->load->view('halaman', $konten);
    }

    public function getData()
    {
        /*
             * list data
             */
        if (IS_AJAX) {
            $sTablex = "";
            $order = "";
            $sTable = 'm_' . $this->uri->segment(3); // m_barang
            $k = '';
            switch ($this->uri->segment(3)):
                case 'pekerjaan':
                    $aColumns = array("kode", "pekerjaan", "tonase", "harga_satuan", "opsi");
                    $kolom = "kode, pekerjaan, tonase, CONCAT('Rp. ',fRupiah(harga_satuan)) as harga_satuan";
                    $where = "1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'vendor':
                    $aColumns = array("kode", "nama_vendor", "opsi");
                    $kolom = "kode, nama_vendor";
                    $where = "1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'jenis':
                    $aColumns = array("kode", "nama_jenis", "singkatan", "opsi");
                    $kolom = "kode, nama_jenis, singkatan";
                    $where = "1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'user':
                    $aColumns = array("kode", "nama_lengkap", "username", "role", "opsi");
                    $kolom = "kode, nama_lengkap, username,CASE WHEN role = '1' THEN 'Admin' WHEN role = '2' THEN 'Marketing' WHEN role = '3' THEN 'Operasional' WHEN role = '4' THEN 'Budgeting' WHEN role ='5'  THEN 'Administrasi' ELSE 'Keuangan' END  AS role";
                    $where = "1=1";
                    $sIndexColumn = "kode";
                    break;
                default:

                    break;
            endswitch;
            if (isset($kolom) && strlen($kolom) > 0) {
                $tQuery = "SELECT $kolom ,'$k' AS opsi  "
                    . "FROM $sTable a $sTablex WHERE $where ";
                echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
            } else {
                echo "";
            }
        }
    }

    public function loadForm()
    {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) != '-') {
            $nilai = $this->uri->segment(4);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                if ($this->uri->segment(6) == '' || strlen(trim($this->uri->segment(6))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(6)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            } else {
                if ($this->uri->segment(5) == '' || strlen(trim($this->uri->segment(5))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(5)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            }
            $konten['rowdata'] = $this->Data_model->satuData($tabel, $kondisi);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                $konten['jenis'] = ($this->uri->segment(6) == '') ? 'A' : strtoupper($this->uri->segment(6));
            } else {
                $konten['jenis'] = strtoupper($this->uri->segment(5));
            }
        } else {
            $konten['jenis'] = strtoupper($this->uri->segment(5));
        }
        $this->load->view('form/' . $this->uri->segment(3), $konten);
    }

    public function simpanData()
    {
        $arrdata = array();
        $cid = '';
        $tabel = 'm_' . $this->uri->segment(3);
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) { } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                if ($key == 'cid') {
                    $cid = $value;
                } else {
                    if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("/", $value);
                            $newtgl = $tgl[1] . "/" . $tgl[0] . "/" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                    } else {
                        if ($this->uri->segment(3) == "user") {
                            $arrdata['password'] = md5($this->input->post('password'));
                            $arrdata[$key] = $value;
                        }
                        $arrdata[$key] = $value;
                    }
                }
            }
        }
        if ($cid == "") {
            try {
                $this->Data_model->simpanData($arrdata, $tabel);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            $kondisi = 'kode';
            try {
                $kondisi = array('kode' => $cid);
                echo $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('m_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }
}
