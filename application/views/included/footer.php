<!-- End of Content Wrapper -->
<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?= base_url('login/doOut') ?>">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/admin') ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/admin') ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/bootstrap-growl/bootstrap-growl.min.js"></script>


<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/admin') ?>/js/sb-admin-2.min.js"></script>
<script src="<?= base_url('assets/admin/js/functions.js') ?>" type="text/javascript"></script>

<?php ($js != '') ? $this->load->view($js) : ''; ?>
<script>
    $(document).ready(function() {
        // Get current page URL
        var url = window.location.href;
        // remove # from URL
        // url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
        // remove parameters from URL
        // url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
        // select file name
        // url = url.substr(url.lastIndexOf("/") + 1);
        // If file name not avilable
        if (url == '') {
            url = 'index.html';
        }
        // console.log(url);
        // Loop all menu items
        $('#accordionSidebar li.nav-item').each(function() {
            // console.log(this);
            // select href
            var href = $(this).find('a').attr('href');
            // Check filename 
            if (url == href) {
                // Add active class
                $(this).addClass('active');
            }
        });
    });
</script>
</body>

</html>