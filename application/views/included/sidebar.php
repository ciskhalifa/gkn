<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <?= APP_NAME; ?>
        </div>
    </a>

    <!-- Heading -->
    <div class="sidebar-heading">
        Main
    </div>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('dashboard'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <?php if ($_SESSION['role'] == '1') : ?>
    <!-- Heading -->
    <div class="sidebar-heading">
        Settings
    </div>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('master'); ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Master Data</span></a>
    </li>
    <?php elseif ($_SESSION['role'] == '2') : ?>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('order'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Order</span></a>
    </li>
    <?php elseif ($_SESSION['role'] == '3') : ?>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('antrian'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Antrian</span></a>
    </li>
    <?php elseif ($_SESSION['role'] == '4') : ?>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('budgeting'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Budgeting</span></a>
    </li>
    <?php elseif ($_SESSION['role'] == '6') : ?>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('keuangan'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Keuangan</span></a>
    </li>
    <?php endif; ?>
</ul>
<!-- End of Sidebar -->